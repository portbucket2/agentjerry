using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


public class DeletePlayerPrefs
{
    [MenuItem("Tools/Clear All PlayerPrefs")]
    private static void DeleteAllPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
    }
}
