﻿namespace com.faithstudio.Gameplay
{
    using System.Collections.Generic;
    using UnityEngine;

    [CreateAssetMenu(fileName = "LevelInfo", menuName = "Info Container/Create LevelInfo")]
    public class LevelInfoContainer : ScriptableObject
    {
        #region Custom Variables

        [System.Serializable]
        public struct LevelInfo
        {
            public int themeIndex;
            public int numberOfAvailableQuizQuestion;
        }

        #endregion

        #region Public Variables

        public List<LevelInfo> levelInfos;

        #endregion

        #region Public Callback

#if UNITY_EDITOR

        public void ResetInfos()
        {
            levelInfos = new List<LevelInfo>();
        }

        public void GenerateLevelInfo(
                int t_ThemeIndex,
                int t_NumberOfAvailableQuizQuestion
            )
        {

            levelInfos.Add(new LevelInfo()
            {
                themeIndex = t_ThemeIndex,
                numberOfAvailableQuizQuestion = t_NumberOfAvailableQuizQuestion
            });

        }

#endif

        public bool IsValidLevel(int t_Level)
        {

            if (t_Level >= 0 && t_Level < levelInfos.Count)
            {
                return true;
            }
            else
            {
                Debug.LogError("Invalid Level Index : " + t_Level);
                return false;
            }
        }


        public int GetThemeIndex(int t_Level)
        {

            if (IsValidLevel(t_Level))
            {
                return levelInfos[t_Level].themeIndex;
            }
            else
            {

                return -1;
            }
        }

        public int GetNumberOfAvailableQuiz(int t_Level)
        {

            if (IsValidLevel(t_Level))
            {
                return levelInfos[t_Level].numberOfAvailableQuizQuestion;
            }
            else
            {

                return -1;
            }
        }

        #endregion
    }

}

