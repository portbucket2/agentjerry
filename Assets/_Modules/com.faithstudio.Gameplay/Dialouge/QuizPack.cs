﻿
using UnityEngine;

#region Custom Variables    :   Global

[System.Serializable]
public struct QuizAnswer
{
    public bool     isRightAnswer;
    public string   answerText;
}

[System.Serializable]
public struct QuizQuestion
    {
        public Sprite       quizLogo;
        public string       quizQuestion;
        public QuizAnswer[] quizProbableAnswers;
    }

#endregion

[CreateAssetMenu(fileName ="Quiz", menuName = "CustomScriptableObject/Quiz")]
public class QuizPack : ScriptableObject
{
    #region Public Variables

    public QuizQuestion[] quizQuestions;

    #endregion   

    #region Public Callback

    public bool IsValudQuizQuestion(int t_QuizIndex){

        if (t_QuizIndex >= 0 && t_QuizIndex < quizQuestions.Length)
            return true;

        Debug.LogError("Invalid QuizIndex : " + t_QuizIndex + ", Must be with in [0," + quizQuestions.Length + ")");
        return false;
    }

    public bool IsValidQuizAnswer(int t_QuizIndex, int t_QuizAnswerIndex){

        if(IsValudQuizQuestion(t_QuizIndex) && (t_QuizAnswerIndex >= 0 && t_QuizAnswerIndex < quizQuestions[t_QuizIndex].quizProbableAnswers.Length))
            return true;
        
        return false;
    }

    public QuizQuestion GetQuizQuestion(int t_QuizIndex){

        if(IsValudQuizQuestion(t_QuizIndex))
            return quizQuestions[t_QuizIndex];

        return new QuizQuestion();
    }

    public Sprite GetQuizLogo(int t_QuizIndex){

        if(IsValudQuizQuestion(t_QuizIndex)){

            return quizQuestions[t_QuizIndex].quizLogo;
        }

        return null;
    }

    public string GetQuizQuestionAsText(int t_QuizIndex){

        if(IsValudQuizQuestion(t_QuizIndex)){

            return quizQuestions[t_QuizIndex].quizQuestion;
        }

        return "";
    }

    public QuizAnswer[] GetQuizAnswers(int t_QuizIndex){

        if(IsValudQuizQuestion(t_QuizIndex))
            return quizQuestions[t_QuizIndex].quizProbableAnswers;

        return null;
    }

    public QuizAnswer GetQuizAnswer(int t_QuizIndex, int t_QuizAnswerIndex){

        if(IsValidQuizAnswer(t_QuizIndex, t_QuizAnswerIndex))
            return quizQuestions[t_QuizIndex].quizProbableAnswers[t_QuizAnswerIndex];
        
        return new QuizAnswer();
    }

    public int GetNumberOfProbableAnswer(int t_QuizIndex){

        if(IsValudQuizQuestion(t_QuizIndex)){

            return quizQuestions[t_QuizIndex].quizProbableAnswers.Length;
        }

        return 0;
    }

    #endregion
}
