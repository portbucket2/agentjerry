﻿namespace com.alphapotato.Gameplay {

    using System.Collections.Generic;
    using System.Collections;
    using UnityEditor;
    using UnityEngine.SceneManagement;
    using UnityEngine;

    public class AdditiveSceneManager : MonoBehaviour {

        #region OnEditorMethod

#if UNITY_EDITOR

        public void CreateListOfAddtiveSceneInfo () {

            if (listOfAdditiveScene == null)
                listOfAdditiveScene = new List<AdditiveSceneInfo> ();
        }

        public void CreateAnEmptyAdditiveSceneInfo () {

            listOfAdditiveScene.Add (new AdditiveSceneInfo ());

            int t_NumberOfActiveScene = listOfAdditiveScene.Count;

            for (int i = 0; i < t_NumberOfActiveScene; i++) {

                if (i == (t_NumberOfActiveScene - 1)) {

                    listOfAdditiveScene[i].isSceneEnabledInBuild = true;
                    listOfAdditiveScene[i].sceneMapForEnable = new List<bool> ();

                    for (int j = 0; j < t_NumberOfActiveScene; j++) {
                        if (i == j) {
                            listOfAdditiveScene[i].sceneMapForEnable.Add (true);
                        } else {
                            listOfAdditiveScene[i].sceneMapForEnable.Add (false);
                        }
                    }
                } else {
                    listOfAdditiveScene[i].sceneMapForEnable.Add (false);
                }
            }
        }

        private bool IsValidScene (int t_SceneIndex) {

            return SceneManager.GetSceneByPath (listOfAdditiveScene[t_SceneIndex].scenePath).IsValid ();
        }

        public string GetSceneName (int t_SceneIndex) {

            return listOfAdditiveScene[t_SceneIndex].sceneName;

        }

#endif

        #endregion

        #region Custom Variables

        public enum SceneTypeForLoading {
            Default,
            RemoveAllOtherScene,
            Custom
        }

        [System.Serializable]
        public class AdditiveSceneInfo {

            #region Public Variables

#if UNITY_EDITOR

            public bool showSceneAsset;
            public bool showSceneSettingsForScene;
            public bool showBuildSettingsForScene;
            public bool isSceneEnabledInBuild;

#endif

            [SerializeField]
            public string scenePath;
            public string sceneName;
            public SceneTypeForLoading sceneTypeForLoading = SceneTypeForLoading.Default;
            public List<bool> sceneMapForEnable;

            #endregion

            #region Public Callback

            float t_LoadingProgression;

            public IEnumerator ControllerForLoadingScene () {

                WaitForSecondsRealtime t_CycleDelay = new WaitForSecondsRealtime (0.0167f);

                float t_LoadingProgression = 0;
                AsyncOperation t_AsyncOperationForLoadScene = SceneManager.LoadSceneAsync (sceneName, LoadSceneMode.Additive);
                t_AsyncOperationForLoadScene.allowSceneActivation = false;

                while (!t_AsyncOperationForLoadScene.isDone) {

                    t_LoadingProgression = t_AsyncOperationForLoadScene.progress;

                    if (t_LoadingProgression >= 0.9f) {
                        break;
                    }

                    yield return t_CycleDelay;
                }

                t_AsyncOperationForLoadScene.allowSceneActivation = true;
            }

            #endregion

        }

        #endregion

        #region Public Variables

        public RenderSettings renderSettings;

        [HideInInspector]
        public List<AdditiveSceneInfo> listOfAdditiveScene;

        #endregion

        #region Private Variables

        #endregion

        #region Mono Behaviour

        private void Awake () {

        }

        #endregion

        #region Public Configuretion

        public void ProcessOnLoadScene (int t_SceneIndex) {

#if UNITY_EDITOR

            if (EditorApplication.isPlaying) {
                StartCoroutine (listOfAdditiveScene[t_SceneIndex].ControllerForLoadingScene ());
            } else {
                UnityEditor.SceneManagement.EditorSceneManager.OpenScene (listOfAdditiveScene[t_SceneIndex].scenePath, UnityEditor.SceneManagement.OpenSceneMode.Additive);
            }

#else

            StartCoroutine (listOfAdditiveScene[t_SceneIndex].ControllerForLoadingScene ());

#endif
        }

        public void ProcessOnUnloadScene (int t_SceneIndex) {

            #if UNITY_EDITOR

            if (EditorApplication.isPlaying) {
                SceneManager.UnloadSceneAsync (listOfAdditiveScene[t_SceneIndex].sceneName);
            } else {
                UnityEditor.SceneManagement.EditorSceneManager.CloseScene (SceneManager.GetSceneByPath(listOfAdditiveScene[t_SceneIndex].scenePath),false);
            }

#else

            SceneManager.UnloadSceneAsync (listOfAdditiveScene[t_SceneIndex].sceneName);

#endif
        }

        #endregion

        #region Public Callback

        public bool IsSceneLoaded(int t_SceneIndex){

            return SceneManager.GetSceneByName (listOfAdditiveScene[t_SceneIndex].sceneName).isLoaded;
        }

        public void LoadScene (int t_SceneIndex) {
            
            List<bool> t_SceneToBeLoaded = listOfAdditiveScene[t_SceneIndex].sceneMapForEnable;
            int t_NumberOfSceneToBeLoaded = t_SceneToBeLoaded.Count;
            for (int i = 0; i < t_NumberOfSceneToBeLoaded; i++) {

                switch (listOfAdditiveScene[t_SceneIndex].sceneTypeForLoading) {

                    case SceneTypeForLoading.Default:

                        if (i == t_SceneIndex && !IsSceneLoaded(i)) {

                            ProcessOnLoadScene(i);
                        }

                        break;

                    case SceneTypeForLoading.RemoveAllOtherScene:

                        if (i == t_SceneIndex && !IsSceneLoaded(i)) {

                            ProcessOnLoadScene(i);

                        } else if (i != t_SceneIndex && IsSceneLoaded(i)) {
                            
                            ProcessOnUnloadScene(i);
                        }

                        break;

                    case SceneTypeForLoading.Custom:

                        if (t_SceneToBeLoaded[i] && !IsSceneLoaded(i)) {

                            ProcessOnLoadScene(i);
                        } else if (!t_SceneToBeLoaded[i] && IsSceneLoaded(i)) {

                            ProcessOnUnloadScene(i);
                        }

                        break;
                }

            }
        }

        public void LightingSettingsManupulation () {

        }

        #endregion
    }
}