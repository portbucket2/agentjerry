﻿namespace com.alphapotato.Gameplay
{
    using UnityEditor;
    using UnityEngine;
    using UnityEngine.Events;
    using System.Collections;
    using com.faithstudio.ScriptedParticle;
    public class ItemFactory : MonoBehaviour
    {
        #region Custom Data

        [System.Serializable]
        public class ItemSkill
        {

            #region Public Variables

            [Header("Parameter  :   DefaultSkillState")]
            public bool isUnlockedInitially;
            public string skillName;
            public string nameOfTransitionKeyForAnimation;
            [Range(0f, 5f)]
            public float delayOnSkillExection;
            [Range(0f, 60f)]
            public float durationForCooldown;
            [Space(5.0f)]
            public Sprite itemSkillDisplayPicture;
            public ParticleController[] itemGlaceOnPowerCharged;

            [Header("Parameter  :   UpgradeState")]
            [Range(1, 100)]
            public int maxSkillLevel = 1;
            [Tooltip("Which will be over base state")]
            public bool stateIncreaseOnBaseValue;
            [Range(0f, 2f)]
            public float stateMultiplier = 0.1f;

            [Space(5.0f)]
            public bool cooldownReduceOnBaseValue = true;
            [Range(0f, 0.25f)]
            public float cooldownReductionByUpgrade;
            [Range(0f, 5f)]
            public float minimumCooldown = 0.4f;

            [Header("Parameter  :   UpgradeCost")]
            public bool costIncreaseOnBaseValue = true;
            [Range(0, 10000000)]
            public double baseCostForUnlock = 0;
            [Range(0f, 1f)]
            public float costIncreaseFactor = 0.05f;


            #endregion

            #region Private Variables

            private bool m_IsSkillExecutionRunning = false;
            private bool m_IsSkillAllowedToExecute = true;

            private string m_PrefKeyForSkill;
            private string m_PrefKeyForSkillUnlocked;
            private string m_PrefKeyForSkillLevel;

            private float m_BaseStateForItem;

            #endregion

            #region Public Callback

            public void DataInitialization(string t_ItemGroupTag, string t_ItemName, float t_BaseStateForItem)
            {

                m_PrefKeyForSkill = "PlayerPref_" + t_ItemGroupTag + "_" + t_ItemName + "_" + skillName;
                m_PrefKeyForSkillUnlocked = m_PrefKeyForSkill + "_IsSkillUnlocked";
                m_PrefKeyForSkillLevel = m_PrefKeyForSkill + "_SkillLevel";

                m_BaseStateForItem = t_BaseStateForItem;

                if (isUnlockedInitially && !IsSkillUnlocked())
                    UnlockSkill();
            }

            public bool IsValidSkillLevel(int t_SkillLevel)
            {

                if (t_SkillLevel >= 0 && t_SkillLevel < maxSkillLevel)
                    return true;

                Debug.LogError("Invalid SkillLevel : " + t_SkillLevel + ", Should be between [0," + maxSkillLevel + ")");
                return false;
            }

            public bool IsSkillUnlocked()
            {

                if (PlayerPrefs.GetInt(m_PrefKeyForSkillUnlocked) == 0)
                    return false;

                return true;
            }

            public float GetCurrentSkillState()
            {

                return GetSkillStateAtLevel(GetCurrentLevelOfSkill());
            }

            public float GetSkillStateAtLevel(int t_LevelIndex)
            {

                float t_CurrentState = m_BaseStateForItem;

                if (IsValidSkillLevel(t_LevelIndex))
                {
                    
                    t_CurrentState = m_BaseStateForItem;

                    for(int i = 0 ;i < t_LevelIndex; i++){

                        if (stateIncreaseOnBaseValue)
                            t_CurrentState += (m_BaseStateForItem * stateMultiplier);
                        else
                            t_CurrentState += (t_CurrentState * stateMultiplier);
                        }
                }

                return t_CurrentState;
            }

            public double GetUpgradeCostForSkillUpgrade()
            {
                double t_CostForUpgrade = Mathf.Infinity;
                if (IsSkillUnlocked())
                {

                    int t_UpgradeLevel = GetCurrentLevelOfSkill() + 1;
                    if (t_UpgradeLevel <= maxSkillLevel)
                    {

                        t_CostForUpgrade = baseCostForUnlock;
                        for (int i = 0 + (isUnlockedInitially ? 1 : 0); i < t_UpgradeLevel; i++)
                        {

                            if (costIncreaseOnBaseValue)
                            {
                                t_CostForUpgrade += (baseCostForUnlock * costIncreaseFactor);
                            }
                            else
                            {
                                t_CostForUpgrade += (t_CostForUpgrade * costIncreaseFactor);
                            }
                        }
                    }
                }else{

                    Debug.LogError("Failed To Upgrade : Item (" + skillName + ") is not purchased yet");
                }
                
                return t_CostForUpgrade;
            }

            public int GetCurrentLevelOfSkill()
            {

                return PlayerPrefs.GetInt(m_PrefKeyForSkillLevel, 0);
            }

            public float GetLevelProgressionOfSkill()
            {

                return GetCurrentLevelOfSkill() / ((float)maxSkillLevel);
            }

            public float GetCurrentCooldownOfSkill()
            {

                return GetCooldownOfSkillAtLevel(GetCurrentLevelOfSkill());
            }

            public float GetCooldownOfSkillAtLevel(int t_SkillLevel)
            {

                float t_Cooldown = 1000;
                if (IsValidSkillLevel(t_SkillLevel))
                {

                    t_Cooldown = durationForCooldown;
                    float t_MinimumCooldownCanBe = durationForCooldown * minimumCooldown;
                    for (int i = 0; i < t_SkillLevel; i++)
                    {

                        if (cooldownReduceOnBaseValue)
                            t_Cooldown -= (durationForCooldown * cooldownReductionByUpgrade);
                        else
                            t_Cooldown -= (t_Cooldown * cooldownReductionByUpgrade);

                        if (t_Cooldown < t_MinimumCooldownCanBe)
                        {
                            t_Cooldown = t_MinimumCooldownCanBe;
                            break;
                        }
                    }
                }

                return t_Cooldown;
            }

            public void UnlockSkill()
            {
                PlayerPrefs.SetInt(m_PrefKeyForSkillUnlocked, 1);
            }

            public void UpgradeSkill()
            {

                int t_CurrentLevelOfSkill = GetCurrentLevelOfSkill();
                if ((t_CurrentLevelOfSkill + 1) < maxSkillLevel)
                {
                    PlayerPrefs.SetInt(m_PrefKeyForSkillLevel, t_CurrentLevelOfSkill + 1);
                }
            }

            public void DowngradeSkill()
            {
                int t_CurrentLevelOfSkill = GetCurrentLevelOfSkill();
                if ((t_CurrentLevelOfSkill - 1) >= 0)
                {
                    PlayerPrefs.SetInt(m_PrefKeyForSkillLevel, t_CurrentLevelOfSkill - 1);
                }
            }

            public void ResetSkill()
            {
                PlayerPrefs.SetInt(m_PrefKeyForSkillUnlocked, 0);
                PlayerPrefs.SetInt(m_PrefKeyForSkillLevel, 0);
            }

            public bool IsSkillExecutionRunning()
            {
                return m_IsSkillExecutionRunning;
            }

            public IEnumerator ControllerForSkillExecution(
                Animator t_WeaponAnimator,
                UnityAction OnSkillExecutionEnd = null,
                UnityAction OnCooldownEnd = null,
                UnityAction<float> OnProgressionWithCooldown = null)
            {

                m_IsSkillExecutionRunning = true;

                float t_CycleLength = 0.033f;
                WaitForSeconds t_CycleDelay = new WaitForSeconds(t_CycleLength);

                t_WeaponAnimator.SetTrigger(nameOfTransitionKeyForAnimation);

                float t_DurationForSkillCooldown = GetCurrentCooldownOfSkill();
                float t_RemainingTimeForSkillCooldown = t_DurationForSkillCooldown;
                float t_RemainingDelayForSkillExecution = delayOnSkillExection;


                while (t_RemainingDelayForSkillExecution > 0)
                {
                    yield return t_CycleDelay;
                    t_RemainingTimeForSkillCooldown -= t_CycleLength;
                    t_RemainingDelayForSkillExecution -= t_CycleLength;
                    OnProgressionWithCooldown?.Invoke(1f - (t_RemainingTimeForSkillCooldown / t_DurationForSkillCooldown));
                }

                OnSkillExecutionEnd?.Invoke();

                while (t_RemainingTimeForSkillCooldown > 0)
                {

                    yield return t_CycleDelay;
                    t_RemainingTimeForSkillCooldown -= t_CycleLength;
                    OnProgressionWithCooldown?.Invoke(1f - (t_RemainingTimeForSkillCooldown / t_DurationForSkillCooldown));
                }

                OnCooldownEnd?.Invoke();

                m_IsSkillExecutionRunning = false;
            }

            #endregion

        }

        [System.Serializable]
        public class ItemData
        {

            #region Public Variables

            [Header("Parameter  :   DefaultItemState")]
            public bool isUnlockedInitially;
            public string itemName;
            public GameObject itemMesh;
            public Animator itemAnimator;
            [Range(1f, 1000f)]
            public float itemState = 1f;
            public Sprite itemDisplayPicture;
            public ParticleController[] itemGlaceOnDefault;

            [Header("Parameter  :   UpgradeState")]
            public bool stateIncreaseOnBaseValue = true;
            [Range(0f, 1f)]
            public float stateIncreaseFactor = 0.05f;
            [Range(1, 100)]
            public int maxLevelToUpgrade = 1;

            [Header("Parameter  :   UpgradeCost")]
            public bool costIncreaseOnBaseValue = true;
            [Range(0, 10000000)]
            public double baseCostForPurchase = 0;
            [Range(0f, 1f)]
            public float costIncreaseFactor = 0.05f;

            [Space(5.0f)]
            public ItemSkill[] itemSkills;

            #endregion

            #region Private Variables

            private string m_PrefKeyForItem;
            private string m_PrefKeyForItemPurchased;
            private string m_PrefKeyForItemLevel;

            private string m_ItemGroupTag;

            #endregion

            #region Public Callback

            public void DataInitialization(string t_ItemGroupTag)
            {

                m_PrefKeyForItem = "PrefKey_" + t_ItemGroupTag + "_" + itemName;
                m_PrefKeyForItemPurchased = m_PrefKeyForItem + "_IsPurchased";
                m_PrefKeyForItemLevel = m_PrefKeyForItem + "_Level";

                m_ItemGroupTag = t_ItemGroupTag;

                if (isUnlockedInitially && !IsItemPurchased())
                    PurchaseItem();

                int t_NumberOfItemSkill = itemSkills.Length;
                for (int i = 0; i < t_NumberOfItemSkill; i++)
                {

                    itemSkills[i].DataInitialization(t_ItemGroupTag, itemName, GetCurrentItemState());
                }
            }

            public bool IsValidItemLevel(int t_ItemLevel)
            {

                if (t_ItemLevel >= 0 && t_ItemLevel <= maxLevelToUpgrade)
                    return true;

                Debug.LogError("Invalid ItemLevel : " + t_ItemLevel + ", Should be between [0," + maxLevelToUpgrade + ")");
                return false;
            }

            public bool IsValidSkillIndex(int t_SkillIndex)
            {

                if (t_SkillIndex >= 0 && t_SkillIndex < itemSkills.Length)
                    return true;

                Debug.LogError("Invalid SkillIndex : " + t_SkillIndex + ", Should be between [0," + itemSkills.Length + ")");
                return false;
            }

            public bool IsItemPurchased()
            {

                if (PlayerPrefs.GetInt(m_PrefKeyForItemPurchased) == 0)
                    return false;

                return true;
            }

            public double GetUpgradeCostForItem()
            {
                double t_CostForUpgrade = Mathf.Infinity;
                if (IsItemPurchased())
                {

                    int t_UpgradeLevel = GetCurrentLevelOfItem() + 1;
                    if (t_UpgradeLevel <= maxLevelToUpgrade)
                    {

                        t_CostForUpgrade = baseCostForPurchase;
                        for (int i = (0 + (isUnlockedInitially ? 1 : 0)); i < t_UpgradeLevel; i++)
                        {

                            if (costIncreaseOnBaseValue)
                            {
                                t_CostForUpgrade += (baseCostForPurchase * costIncreaseFactor);
                            }
                            else
                            {
                                t_CostForUpgrade += (t_CostForUpgrade * costIncreaseFactor);
                            }
                        }
                    }
                }else{

                    Debug.LogError("Failed To Upgrade : Item (" + itemName + ") is not purchased yet");
                }
                
                return t_CostForUpgrade;
            }

            public int GetCurrentLevelOfItem()
            {

                return PlayerPrefs.GetInt(m_PrefKeyForItemLevel, 0);
            }

            public float GetLevelProgressionOfItem()
            {

                return ((GetCurrentLevelOfItem()) / ((float)maxLevelToUpgrade));
            }

            public float GetCurrentItemState()
            {

                return GetItemStateAtLevel(GetCurrentLevelOfItem());
            }

            public float GetItemStateAtLevel(int t_ItemLevel)
            {

                float t_StateOfItem = 1000;
                if (IsValidItemLevel(t_ItemLevel))
                {

                    t_StateOfItem = itemState;
                    for (int i = 0; i < t_ItemLevel; i++)
                    {

                        if (stateIncreaseOnBaseValue)
                            t_StateOfItem += (itemState * stateIncreaseFactor);
                        else
                            t_StateOfItem += (t_StateOfItem * stateIncreaseFactor);
                    }
                }

                return t_StateOfItem;
            }

            public void PurchaseItem()
            {
                PlayerPrefs.SetInt(m_PrefKeyForItemPurchased, 1);
            }

            public void UpgradeItem()
            {

                int t_CurrentLevelOfItem = GetCurrentLevelOfItem();
                if ((t_CurrentLevelOfItem + 1) <= maxLevelToUpgrade)
                {
                    PlayerPrefs.SetInt(m_PrefKeyForItemLevel, t_CurrentLevelOfItem + 1);

                    int t_NumberOfItemSkill = itemSkills.Length;
                    for (int i = 0; i < t_NumberOfItemSkill; i++)
                    {
                        itemSkills[i].DataInitialization(m_ItemGroupTag, itemName, GetCurrentItemState());
                    }

                    ShowDefaultItemGlace(true);
                }
            }

            public void DowngradeItem()
            {

                int t_CurrentLevelOfItem = GetCurrentLevelOfItem();
                if ((t_CurrentLevelOfItem - 1) >= 0)
                {
                    PlayerPrefs.SetInt(m_PrefKeyForItemLevel, t_CurrentLevelOfItem - 1);
                }

                ShowDefaultItemGlace(false);
            }

            public void ResetItemPurchase(){

                PlayerPrefs.SetInt(m_PrefKeyForItemPurchased, 0);
            }

            public void ResetItemLevel()
            {

                PlayerPrefs.SetInt(m_PrefKeyForItemLevel, 0);
                ShowDefaultItemGlace(false);
            }

            public void ResetItem(){

                ResetItemPurchase();
                ResetItemLevel();
            }

            public void ShowDefaultItemGlace(bool t_IsIncreaseTheValue)
            {
                float t_ValueOfGlace = GetLevelProgressionOfItem();
                Debug.Log("ValueOfGlace : " + t_ValueOfGlace);

                int t_NumberOfParticleControllerReferenceForDefaultItemGlace = itemGlaceOnDefault.Length;
                for (int i = 0; i < t_NumberOfParticleControllerReferenceForDefaultItemGlace; i++)
                {
                    itemGlaceOnDefault[i].PlayParticle();
                    
                    if(t_IsIncreaseTheValue)
                        itemGlaceOnDefault[i].LerpOnPreset(t_ValueOfGlace);
                    else
                        itemGlaceOnDefault[i].LerpOnDefault(t_ValueOfGlace);
                }
            }

            #endregion
        }

        #endregion

        #region Public Variables

        public string itemGroupTag;
        public ItemData[] itemDatas;

        #endregion

        #region Mono Behaviour

        private void Awake()
        {
            DataInitialization();
        }

        #endregion

        #region Configuretion

        public void DataInitialization()
        {
            if (itemDatas != null)
            {

                int t_NumberOfItemData = itemDatas.Length;
                for (int i = 0; i < t_NumberOfItemData; i++)
                {

                    itemDatas[i].DataInitialization(itemGroupTag);
                }
            }

        }

        private bool IsValidItemIndex(int t_ItemIndex)
        {

            if (t_ItemIndex >= 0 && t_ItemIndex < itemDatas.Length)
                return true;

            Debug.Log("Invalid ItemIndex : " + t_ItemIndex + ", Should be between [0," + itemDatas.Length + ")");
            return false;
        }

        private string GetPrefKeyForSelectedItem()
        {

            return "PrefKey_" + itemGroupTag + "_SelectionIndex";
        }

        #endregion

        #region Public Variables   :   ItemFactory

        public int GetSelectedItemIndex()
        {

            return PlayerPrefs.GetInt(GetPrefKeyForSelectedItem(), 0);
        }

        public void SetSelectedItemIndex(int t_ItemIndex)
        {

            if (IsValidItemIndex(t_ItemIndex))
                PlayerPrefs.SetInt(GetPrefKeyForSelectedItem(), t_ItemIndex);
        }

        public void GoToNextItem()
        {

            int t_NewItemIndex = GetSelectedItemIndex() + 1;

            if (t_NewItemIndex >= itemDatas.Length)
            {

                t_NewItemIndex = 0;
            }

            PlayerPrefs.SetInt(GetPrefKeyForSelectedItem(), t_NewItemIndex);
            EnableCurrentItem();
        }

        public void GoToPreviousItem()
        {

            int t_NewItemIndex = GetSelectedItemIndex() - 1;

            if (t_NewItemIndex < 0)
            {

                t_NewItemIndex = itemDatas.Length - 1;
            }

            PlayerPrefs.SetInt(GetPrefKeyForSelectedItem(), t_NewItemIndex);
            EnableCurrentItem();
        }

        public void EnableCurrentItem()
        {

            int t_SelectedCharacterIndex = GetSelectedItemIndex();
            int t_NumberOfItem = itemDatas.Length;
            for (int i = 0; i < t_NumberOfItem; i++)
            {

                if (i == t_SelectedCharacterIndex)
                {
                    itemDatas[i].itemMesh.SetActive(true);

                    #if UNITY_EDITOR

                    if(EditorApplication.isPlaying){
                        itemDatas[i].ShowDefaultItemGlace(true);
                    }

                    #else
                    
                    itemDatas[i].ShowDefaultItemGlace(true);

                    #endif

                    
                }else
                {
                    itemDatas[i].itemMesh.SetActive(false);

                    #if UNITY_EDITOR

                    if(EditorApplication.isPlaying){
                        itemDatas[i].ShowDefaultItemGlace(false);
                    }

                    #else
                    
                    itemDatas[i].ShowDefaultItemGlace(false);

                    #endif
                }
            }
        }



        #endregion

        #region Public Callback    :   ItemData

        public string GetSelectedItemName()
        {

            return GetItemName(GetSelectedItemIndex());
        }

        public string GetItemName(int t_ItemIndex)
        {

            if (IsValidItemIndex(t_ItemIndex))
                return itemDatas[t_ItemIndex].itemName;

            return "Item Has No Name";
        }

        public GameObject GetSelectedItemMesh()
        {
            return GetItemMesh(GetSelectedItemIndex());
        }

        public GameObject GetItemMesh(int t_ItemIndex)
        {

            if (IsValidItemIndex(t_ItemIndex))
                return itemDatas[t_ItemIndex].itemMesh;

            return null;
        }

        public Animator GetSelectedItemAnimator()
        {

            return GetItemAnimator(GetSelectedItemIndex());
        }

        public Animator GetItemAnimator(int t_ItemIndex)
        {

            if (IsValidItemIndex(t_ItemIndex))
                return itemDatas[t_ItemIndex].itemAnimator;

            return null;
        }

        public float GetSelectedItemBaseState()
        {

            return GetItemBaseState(GetSelectedItemIndex());
        }

        public float GetItemBaseState(int t_ItemIndex)
        {

            if (IsValidItemIndex(t_ItemIndex))
                return itemDatas[t_ItemIndex].itemState;

            return 0;
        }

        public Sprite GetSelectedItemDisplayPicture()
        {

            return GetItemDisplayPicture(GetSelectedItemIndex());
        }

        public Sprite GetItemDisplayPicture(int t_ItemIndex)
        {

            if (IsValidItemIndex(t_ItemIndex))
                return itemDatas[t_ItemIndex].itemDisplayPicture;

            return null;
        }

        public ParticleController[] GetSelectedItemGlaceOnDefault()
        {

            return GetItemGlaceOnDefault(GetSelectedItemIndex());
        }

        public ParticleController[] GetItemGlaceOnDefault(int t_ItemIndex)
        {

            if (IsValidItemIndex(t_ItemIndex))
                return itemDatas[t_ItemIndex].itemGlaceOnDefault;

            return null;
        }

        public int GetSelectedItemLevel()
        {

            return GetItemLevel(GetSelectedItemIndex());
        }

        public int GetItemLevel(int t_ItemIndex)
        {

            if (IsValidItemIndex(t_ItemIndex))
                return itemDatas[t_ItemIndex].GetCurrentLevelOfItem();

            return 0;
        }

        public int GetSelectedItemMaxLevel()
        {

            return GetItemMaxLevel(GetSelectedItemIndex());
        }

        public int GetItemMaxLevel(int t_ItemIndex)
        {

            if (IsValidItemIndex(t_ItemIndex))
                return itemDatas[t_ItemIndex].maxLevelToUpgrade;

            return 0;
        }

        public float GetSelectedItemLevelProgression()
        {

            return GetItemLevelProgression(GetSelectedItemIndex());
        }

        public float GetItemLevelProgression(int t_ItemIndex)
        {

            if (IsValidItemIndex(t_ItemIndex))
                return itemDatas[t_ItemIndex].GetLevelProgressionOfItem();

            return 0;
        }

        public int GetNumberOfSkillForSelectedItem()
        {

            return GetNumberOfSkillForItem(GetSelectedItemIndex());
        }

        public int GetNumberOfSkillForItem(int t_ItemIndex)
        {

            if (IsValidItemIndex(t_ItemIndex))
                return itemDatas[t_ItemIndex].itemSkills.Length;

            return 0;
        }

        public float GetCurrentStateOfSelectedItem()
        {
            int t_SelectedItemIndex = GetSelectedItemIndex();
            int t_SelectedItemLevel = itemDatas[t_SelectedItemIndex].GetCurrentLevelOfItem();
            return GetCurrentStateOfItemAtLevel(t_SelectedItemIndex, t_SelectedItemLevel);
        }

        public float GetCurrentStateOfItem(int t_ItemIndex)
        {

            int t_SelectedItemLevel = itemDatas[t_ItemIndex].GetCurrentLevelOfItem();
            return GetCurrentStateOfItemAtLevel(t_ItemIndex, t_SelectedItemLevel);
        }

        public float GetCurrentStateOfItemAtLevel(int t_ItemIndex, int t_ItemLevel)
        {

            if (IsValidItemIndex(t_ItemIndex) && itemDatas[t_ItemIndex].IsValidItemLevel(t_ItemLevel))
                return itemDatas[t_ItemIndex].GetItemStateAtLevel(t_ItemLevel);

            return 0;
        }

        public bool IsItemPurchased(int t_ItemIndex)
        {

            if (IsValidItemIndex(t_ItemIndex))
                return itemDatas[t_ItemIndex].IsItemPurchased();

            return false;
        }

        public double GetPurchaseCostForItem(int t_ItemIndex)
        {

            if (IsValidItemIndex(t_ItemIndex))
                return itemDatas[t_ItemIndex].baseCostForPurchase;

            return Mathf.Infinity;
        }

        public void PurchaseItem(int t_ItemIndex)
        {

            if (IsValidItemIndex(t_ItemIndex)){

                itemDatas[t_ItemIndex].PurchaseItem();
                SetSelectedItemIndex(t_ItemIndex);
                EnableCurrentItem();
            }
        }

        public double GetUpgradeCostForSelectedItem()
        {

            return GetUpgradeCostForItem(GetSelectedItemIndex());
        }

        public double GetUpgradeCostForItem(int t_ItemIndex)
        {

            if (IsValidItemIndex(t_ItemIndex))
                return itemDatas[t_ItemIndex].GetUpgradeCostForItem();

            return Mathf.Infinity;
        }

        public void UpgradeSelectedItem()
        {

            UpgradeItem(GetSelectedItemIndex());
        }

        public void UpgradeItem(int t_ItemIndex)
        {

            if (IsValidItemIndex(t_ItemIndex))
                itemDatas[t_ItemIndex].UpgradeItem();
        }

        public void DowngradeSelectedItem()
        {

            DowngradeItem(GetSelectedItemIndex());
        }

        public void DowngradeItem(int t_ItemIndex)
        {

            if (IsValidItemIndex(t_ItemIndex))
                itemDatas[t_ItemIndex].DowngradeItem();
        }

        public void ResetSelectedItemPurchase()
        {
            ResetItemPurchase(GetSelectedItemIndex());
        }

        public void ResetItemPurchase(int t_ItemIndex)
        {

            if (IsValidItemIndex(t_ItemIndex))
                itemDatas[t_ItemIndex].ResetItemPurchase();
        }

        public void ResetSelectedItemLevel()
        {

            ResetItemLevel(GetSelectedItemIndex());
        }

        public void ResetItemLevel(int t_ItemIndex)
        {

            if (IsValidItemIndex(t_ItemIndex))
                itemDatas[t_ItemIndex].ResetItemLevel();
        }

        public void ResetSelectedItem()
        {

            ResetItem(GetSelectedItemIndex());
        }

        public void ResetItem(int t_ItemIndex)
        {

            if (IsValidItemIndex(t_ItemIndex))
                itemDatas[t_ItemIndex].ResetItem();
        }

        #endregion

        #region Public Callback    :   ItemSkill

        public string GetSelectedSkillName(int t_SkillIndex = 0)
        {

            return GetSkillName(GetSelectedItemIndex(), t_SkillIndex);
        }

        public string GetSkillName(int t_ItemIndex, int t_SkillIndex)
        {

            if (IsValidItemIndex(t_ItemIndex) && itemDatas[t_ItemIndex].IsValidSkillIndex(t_SkillIndex))
                return itemDatas[t_ItemIndex].itemSkills[t_SkillIndex].skillName;

            return "Item Has No Name";
        }

        public float GetSkillCooldownForSelectedItem(int t_SkillIndex = 0)
        {

            return GetSkillCooldownForItem(GetSelectedItemIndex(), t_SkillIndex);
        }

        public float GetSkillCooldownForItem(int t_ItemIndex, int t_SkillIndex)
        {

            if (IsValidItemIndex(t_ItemIndex) && itemDatas[t_ItemIndex].IsValidSkillIndex(t_SkillIndex))
                return itemDatas[t_ItemIndex].itemSkills[t_SkillIndex].durationForCooldown;

            return 0;
        }

        public Sprite GetSelectedItemDisplayPicture(int t_SkillIndex = 0)
        {

            return GetItemSkillDisplayPicture(GetSelectedItemIndex(), t_SkillIndex);
        }

        public Sprite GetItemSkillDisplayPicture(int t_ItemIndex, int t_SkillIndex)
        {

            if (IsValidItemIndex(t_ItemIndex) && itemDatas[t_ItemIndex].IsValidSkillIndex(t_SkillIndex))
                return itemDatas[t_ItemIndex].itemSkills[t_SkillIndex].itemSkillDisplayPicture;

            return null;
        }

        public int GetSkillLevelForSelectedItem(int t_SkillIndex){
            
            return GetSkillLevel(GetSelectedItemIndex(), t_SkillIndex);
        }

        public int GetSkillLevel(int t_ItemIndex, int t_SkillIndex){

             if (IsValidItemIndex(t_ItemIndex) && itemDatas[t_ItemIndex].IsValidSkillIndex(t_SkillIndex))
                return itemDatas[t_ItemIndex].itemSkills[t_SkillIndex].GetCurrentLevelOfSkill();

            return 0;
        }

        public int GetSelectedItemMaxSkillLevel(int t_SkillIndex = 0)
        {

            return GetItemSkillMaxSkillLevel(GetSelectedItemIndex(), t_SkillIndex);
        }

        public int GetItemSkillMaxSkillLevel(int t_ItemIndex, int t_SkillIndex)
        {

            if (IsValidItemIndex(t_ItemIndex) && itemDatas[t_ItemIndex].IsValidSkillIndex(t_SkillIndex))
                return itemDatas[t_ItemIndex].itemSkills[t_SkillIndex].maxSkillLevel;

            return 0;
        }

        public bool IsSkillUnlockedForSelectedItem(int t_SkillIndex)
        {
            return IsSkillUnlocked(GetSelectedItemIndex(), t_SkillIndex);
        }

        public bool IsSkillUnlocked(int t_ItemIndex, int t_SkillIndex)
        {

            if (IsValidItemIndex(t_ItemIndex) && itemDatas[t_ItemIndex].IsValidSkillIndex(t_SkillIndex))
                return itemDatas[t_ItemIndex].itemSkills[t_SkillIndex].IsSkillUnlocked();

            return false;
        }

        public double GetUnlockCostOfSkillForSelectedItem(int t_SkillIndex)
        {

            return GetUnlockCostOfSkill(GetSelectedItemIndex(), t_SkillIndex);
        }

        public double GetUnlockCostOfSkill(int t_ItemIndex, int t_SkillIndex)
        {

            if (IsValidItemIndex(t_ItemIndex) && itemDatas[t_ItemIndex].IsValidSkillIndex(t_SkillIndex))
                return itemDatas[t_ItemIndex].itemSkills[t_SkillIndex].baseCostForUnlock;

            return Mathf.Infinity;
        }

        public void UnlockSkillForSelectedItem(int t_SkillIndex)
        {

            UnlockSkill(GetSelectedItemIndex(), t_SkillIndex);
        }

        public void UnlockSkill(int t_ItemIndex, int t_SkillIndex)
        {
            if (IsValidItemIndex(t_ItemIndex) && itemDatas[t_ItemIndex].IsValidSkillIndex(t_SkillIndex)){
                if(IsItemPurchased(t_ItemIndex))
                    itemDatas[t_ItemIndex].itemSkills[t_SkillIndex].UnlockSkill();
                else
                    Debug.LogError("Failed To Unlock Skill : As Item is not purchased yet");
            }
                
        }


        public float GetSkillStateForSelectedItem(int t_SkillIndex)
        {

            return GetSkillState(GetSelectedItemIndex(), t_SkillIndex);
        }

        public float GetSkillState(int t_ItemIndex, int t_SkillIndex)
        {

            if (IsValidItemIndex(t_ItemIndex) && itemDatas[t_ItemIndex].IsValidSkillIndex(t_SkillIndex))
                return itemDatas[t_ItemIndex].itemSkills[t_SkillIndex].GetCurrentSkillState();

            return 0;
        }

        public double GetUpgradeCostOfSkillForSelectedItem(int t_SkillIndex)
        {
            return GetUpgradeCostOfSkill(GetSelectedItemIndex(), t_SkillIndex);
        }

        public double GetUpgradeCostOfSkill(int t_ItemIndex, int t_SkillIndex)
        {

            if (IsValidItemIndex(t_ItemIndex) && itemDatas[t_ItemIndex].IsValidSkillIndex(t_SkillIndex))
                return itemDatas[t_ItemIndex].itemSkills[t_SkillIndex].GetUpgradeCostForSkillUpgrade();

            return Mathf.Infinity;
        }

        public void UpgradeSkillForSelectedItem(int t_SkillIndex)
        {

            UpgradeSkill(GetSelectedItemIndex(), t_SkillIndex);
        }

        public void UpgradeSkill(int t_ItemIndex, int t_SkillIndex)
        {

            if (IsValidItemIndex(t_ItemIndex) && itemDatas[t_ItemIndex].IsValidSkillIndex(t_SkillIndex))
                itemDatas[t_ItemIndex].itemSkills[t_SkillIndex].UpgradeSkill();
        }

        public void DowngradeSkillForSelectedItem(int t_SkillIndex)
        {

            DowngradeSkill(GetSelectedItemIndex(), t_SkillIndex);
        }

        public void DowngradeSkill(int t_ItemIndex, int t_SkillIndex)
        {

            if (IsValidItemIndex(t_ItemIndex) && itemDatas[t_ItemIndex].IsValidSkillIndex(t_SkillIndex))
                itemDatas[t_ItemIndex].itemSkills[t_SkillIndex].DowngradeSkill();
        }

        public void ResetSkillForSelectedItem(int t_SkillIndex)
        {

            ResetSkill(GetSelectedItemIndex(), t_SkillIndex);
        }

        public void ResetSkill(int t_ItemIndex, int t_SkillIndex)
        {

            if (IsValidItemIndex(t_ItemIndex) && itemDatas[t_ItemIndex].IsValidSkillIndex(t_SkillIndex))
                itemDatas[t_ItemIndex].itemSkills[t_SkillIndex].ResetSkill();
        }

        public void ExecuteSkillForSelectedItem(
            int t_SkillIndex,
            UnityAction OnSkillExecutionEnd = null,
            UnityAction OnCooldownEnd = null,
            UnityAction<float> OnProgressionWithCooldown = null)
        {

            ExecuteSkill(
                GetSelectedItemIndex(),
                t_SkillIndex,
                OnSkillExecutionEnd,
                OnCooldownEnd,
                OnProgressionWithCooldown
            );
        }

        public void ExecuteSkill(
            int t_ItemIndex,
            int t_SkillIndex,
            UnityAction OnSkillExecutionEnd = null,
            UnityAction OnCooldownEnd = null,
            UnityAction<float> OnProgressionWithCooldown = null)
        {

            if (IsValidItemIndex(t_ItemIndex) && itemDatas[t_ItemIndex].IsValidSkillIndex(t_SkillIndex))
            {

                if (!itemDatas[t_ItemIndex].itemSkills[t_SkillIndex].IsSkillExecutionRunning())
                {

                    StartCoroutine(itemDatas[t_ItemIndex].itemSkills[t_SkillIndex].ControllerForSkillExecution(
                        itemDatas[t_ItemIndex].itemAnimator,
                        OnSkillExecutionEnd,
                        OnCooldownEnd,
                        OnProgressionWithCooldown
                    ));
                }
            }
        }

        #endregion
    }

}

