﻿namespace com.alphapotato.Gameplay {

    using System.Collections.Generic;
    using UnityEngine;

    #region Custom Variables

    [SerializeField]
    public class GPSGuidedPath
    {
        #if UNITY_EDITOR
        public Transform editorPathContainer;
        #endif
        
        public Transform[] arrayOfPath;
    }

    #endregion

    public class GPSTrackerController : ExtendedMonoBehaviour {
        
        #region Public Variables

        public List<GPSGuidedPath> listOfGuidedPath;

        #endregion

        #region Private Variables

        private List<Transform> m_ActiveListOfGuidedPath;

        #endregion
    }
}