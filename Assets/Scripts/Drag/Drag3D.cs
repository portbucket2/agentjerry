using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Drag3D : MonoBehaviour,IItem
{
    #region Private Variables
    private float zPosition;
    private Vector3 offset;
    private bool dragging;

    #endregion

    #region Public Variables
    public Camera mainCamera;
    public int ID;
    public Vector3 initPos;
    public Vector3 offsetPos;

    public Vector3 initRotation;
    public Vector3 offsetRotation;

    [Space]
    public ItemDefine itemDefine;
    [Range(0f, 5f)] public float glowIntensity;
    public float floor = 0.3f;
    public float ceiling = 1.0f;

    [Space]
    [Header("Hint Con")]
    public float LastTime;
    public float HintDelay = 5.0f;
    public float lastAnimationTime;
    public Animator itemAnimator;
    public bool isTouchedOnce;

    public bool isSetOnTray;


    [Space]
    public UnityEvent OnbeginDrag;
    public UnityEvent OnEndDrag;
    #endregion


    private void Start()
    {
        zPosition = mainCamera.WorldToScreenPoint(transform.position).z;
        initPos = transform.position;
        initRotation = transform.rotation.eulerAngles;
        LastTime = Time.realtimeSinceStartup;
        itemAnimator = GetComponent<Animator>();
    }

    public void EnableAnimation()
    {
        GetComponent<Animator>().enabled = false;
    }

    private void Update()
    {
        if (dragging)
        {
            Vector3 t_Position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, zPosition);
            Vector3 t_Updated = mainCamera.ScreenToWorldPoint(t_Position + new Vector3(offset.x, offset.y));
            transform.position = new Vector3(t_Updated.x, transform.position.y, t_Updated.z);
            
        }
        //Renderer renderer = GetComponent<Renderer>();
        //Material mat = renderer.material;

        //glowIntensity = floor + Mathf.PingPong(Time.time, ceiling - floor);
        //Color baseColor = Color.white; //Replace this with whatever you want for your base color at emission level '1'

        //Color finalColor = baseColor * Mathf.LinearToGammaSpace(glowIntensity);

        //mat.SetColor("_EmissionColor", finalColor);

        if (!isTouchedOnce)
        {
            if (Time.realtimeSinceStartup - LastTime > HintDelay)
            {
                lastAnimationTime += Time.deltaTime;
                if (lastAnimationTime > 3f)
                {
                    DisplayHint();
                }

            }

            if (Input.GetMouseButtonUp(0))
            {
                LastTime = Time.realtimeSinceStartup;
            }
        }

    }

    private void DisplayHint()
    {
        lastAnimationTime = 0;
        itemAnimator.SetTrigger("move");
        if (!itemAnimator.GetCurrentAnimatorStateInfo(0).IsName("Move"))
        {
            
        }
        
    }

    private void OnMouseDown()
    {
        if (!dragging)
        {
            BeginDrag();
        }
    }

    private void OnMouseUp()
    {
       
        EndDrag();
    }

    private void BeginDrag()
    {
        OnbeginDrag.Invoke();
        dragging = true;
        offset = mainCamera.WorldToScreenPoint(transform.position) - Input.mousePosition;
        isTouchedOnce = true;
    }

    private void EndDrag()
    {
        OnEndDrag.Invoke();
        dragging = false;

        if (!isSetOnTray)
        {
            transform.position = initPos;
        }
    }

    public void SetOnTriggerPos(Vector3 t_Pos)
    {
        GetComponent<Animator>().enabled = false;
        transform.position = t_Pos + offsetPos;
        transform.rotation = Quaternion.Euler (offsetRotation.x,offsetRotation.y,offsetRotation.z);
        dragging = false;
        isSetOnTray = true;
    }

    public void Success()
    {
        UIManager.Instance.ShowLevelComplete();
    }

    public void PlaySuccess()
    {
        GetComponent<Animator>().SetTrigger("success");
    }

    public void ReturnItemToInitPos()
    {
        transform.position = initPos;
        transform.rotation = Quaternion.Euler(initRotation.x, initRotation.y, initRotation.z);
    }
    
}
