using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Gameplay : MonoBehaviour
{
    public static Gameplay Instance;

    public int ansCounter;

    public ItemSlot trayItem;
    public List<Drag3D> drag3DList;


    [Space]
    [Header("TEmp, change with interface")]
    public int itemNumberToSolve;
    public ISolve solvingLevel;


    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        drag3DList = new List<Drag3D>();
        solvingLevel = GetComponent<ISolve>();
        Debug.Log(solvingLevel);
    }
    private void Start()
    {
        //LionKitManager.Instance.LevelStarted(LevelManager.Instance.GetCurrentLevel());
    }
    public void StartGame()
    {
        LionKitManager.Instance.LevelStarted(LevelManager.Instance.GetCurrentLevelTEMP());

        StartCoroutine(StartGameRoutine());
    }
    private IEnumerator StartGameRoutine()
    {
        yield return StartCoroutine(UIManager.Instance.TapButtonRoutine());
        trayItem.AppearTray();

        yield return new WaitForSeconds(0.25f);
        

        solvingLevel.Initialize();
    }

    public void AddToList(Drag3D drag3D)
    {
        drag3DList.Add(drag3D);
        UIManager.Instance.AppearSolveButton();

        if (drag3DList.Count == itemNumberToSolve)
        {
            
        }
    }

    public void SolveItem()
    {
        for (int i = 0; i < drag3DList.Count; i++)
        {
            if(drag3DList[i].itemDefine == ItemDefine.RIGHT)
            {
                ansCounter++;
            }
            else
            {
                //Wrong item on the slot. return the item on initial pos
                drag3DList[i].ReturnItemToInitPos();
                solvingLevel.WrongMessage();
            }
        }
        

        if(ansCounter == itemNumberToSolve)
        {
            //Right Answer all. solve the problem
            Debug.LogError("SOLVELD");
            UIManager.Instance.DisappearSolveButton();
            solvingLevel.SolveLevel();
        }
        else
        {
            drag3DList.Clear();
            UIManager.Instance.DisappearSolveButton();
        }
    }

    public void LevelComplete()
    {
        //LevelManager.Instance.IncreaseGameLevel();
        StartCoroutine(LevelCompleteRoutine());
    }

    private IEnumerator LevelCompleteRoutine()
    {
        yield return new WaitForSeconds(.15f);
        UIManager.Instance.AppearThanksThought();
        yield return new WaitForSeconds(1.5f);
        UIManager.Instance.ShowLevelComplete();
    }

    public void GotoNextLevel()
    {
        if (LevelManager.Instance.GetCurrentLevel() > 3)
        {
            PlayerPrefs.DeleteKey("Game_Level_Pref");
        }

        int t_CurrentLevel = LevelManager.Instance.GetCurrentLevel();

        Debug.Log(t_CurrentLevel + " Scene" );

        
        

        switch (t_CurrentLevel)
        {
            case 1:
                SceneManager.LoadScene("Save yourself"); 
                break;
            case 2:
                SceneManager.LoadScene("Save the world");
                break;
            case 3:
                SceneManager.LoadScene("Escape");
                break;

            default:
                break;
        }
    }
}
