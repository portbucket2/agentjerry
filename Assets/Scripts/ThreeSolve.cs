using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ThreeSolve : MonoBehaviour,ISolve
{
    public ParticleSystem trayParticle;
    public ParticleSystem trayTwoParticle;

    public ParticleSystem itemParticle;
    public GameObject solveItem;

   
    public ItemSlot trayTwo;


    [Space]
    [Header("Agent con")]
    private int THINKING = Animator.StringToHash("thinking");
    private int WRONG = Animator.StringToHash("wrong");
    private int SEEKING_HELP = Animator.StringToHash("seekinghelp");
    private int RUNNING = Animator.StringToHash("running");
    private int ZIP_START = Animator.StringToHash("zipstart");
    private int SUCCESS = Animator.StringToHash("success");
    public Animator agentAnimator;

    public Transform agentTrans;
    public List<Transform> posList;

    public UnityEvent OnMiddleEvents;
    public UnityEvent OnSolveFinish;

    public void Initialize()
    {
       
        trayTwo.AppearTray();

        StartAgentBehaviour();
    }
    public void StartAgentBehaviour()
    {
        InvokeRepeating("AgentBehaviour", 2.0f, 4f);
    }

    private void AgentBehaviour()
    {
        int t_RandValue = Random.Range(0, 2);

        Debug.Log(t_RandValue);
        UIManager.Instance.DisappearItemThought();

        switch (t_RandValue)
        {
            case 0:
                agentAnimator.SetTrigger(THINKING);
                UIManager.Instance.AppearItemThought();
                break;
            case 1:
                agentAnimator.SetTrigger(SEEKING_HELP);
                break;


            default:
                break;
        }
    }

    public void SolveLevel()
    {
        StartCoroutine(SolveRoutine());
    }

    public void WrongMessage()
    {
        agentAnimator.SetTrigger(WRONG);
    }

    private IEnumerator SolveRoutine()
    {
        trayParticle.Play();
        trayTwoParticle.Play();
        yield return new WaitForSeconds(0.1f);

        OnMiddleEvents.Invoke();
        yield return new WaitForSeconds(0.5f);

        itemParticle.Play();
        solveItem.SetActive(true);
        yield return new WaitForSeconds(1f);
        
      

        yield return new WaitForSeconds(0.5f);
        OnSolveFinish.Invoke();

        UIManager.Instance.DisappearItemThought();

        CancelInvoke();
        yield return new WaitForSeconds(0.1f);
        agentAnimator.SetTrigger(SUCCESS);
        yield return new WaitForSeconds(1f);
        UIManager.Instance.AppearThanksThought();
        yield return new WaitForSeconds(1f);
        UIManager.Instance.DisappearThanksThought();


        
        agentTrans.rotation = Quaternion.Euler(0,-360,0);
        agentAnimator.SetTrigger(RUNNING);
        yield return StartCoroutine(
            AgentJerryController.Instance.XAxisMoveRoutine(posList[0].position,
            posList[1].gameObject,delegate {
            agentAnimator.SetTrigger(ZIP_START);}));


        StartCoroutine(XAxisMoveRoutine(solveItem.transform, posList[3].position));

        yield return StartCoroutine(AgentJerryController.Instance.XAxisMoveRoutine(posList[1].position,posList[2].gameObject,delegate
        {
            UIManager.Instance.ShowLevelComplete();

        }));

    }

    public IEnumerator XAxisMoveRoutine(Transform t_TargetTransActual, Vector3 t_DesPos, UnityAction t_Action = null)
    {
        float t_Progression = 0f;
        float t_Duration = 1.5f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = t_TargetTransActual.position;
        Vector3 t_DestValue = t_DesPos;// new Vector3(2.16f,1.589f,-.238f);


        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            t_TargetTransActual.position = new Vector3(
                Mathf.Lerp(t_CurrentFillValue.x, t_DestValue.x, t_Progression),
                Mathf.Lerp(t_CurrentFillValue.y, t_DestValue.y, t_Progression),
                Mathf.Lerp(t_CurrentFillValue.z, t_DestValue.z, t_Progression));

            if (t_Progression >= 1f)
            {
                if (t_Action != null)
                {
                    t_Action.Invoke();
                }

                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(XAxisMoveRoutine(null, Vector3.zero, null));
    }
}
