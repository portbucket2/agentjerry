﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using System.Collections;

public class UIManager : MonoBehaviour
{
    [Header("Start Panel")] public TextMeshProUGUI levelNumberText;
    public Button tapButton;
    public Animator startPanelAnimator;
    
    private int ENTRY = Animator.StringToHash("entry");
    private int EXIT = Animator.StringToHash("exit");
    private static UIManager m_Instance;
    
    [Header("Upper panel")] public Animator upperPanelAnimator;
    public TextMeshProUGUI currentLevelText;
    
    
    [Header("Level Complete")] public Animator levelCompleteAnimator;
    public Button collectButton;
    public ParticleSystem levelCompleteParticle;
    public Image levComBackImage;

    [Space]
    
    public Drag3D dragObject;
    public Drag3D wrongDrag;
    public int currentSelectedID;

    [Space]
    [Header("Objective Panel")]
    public Animator objectiveAnimator;

    [Space]
    [Header("Solve button")]
    public Button solveButton;
    public Animator solveButtonAnimator;
    public GameObject thoughtBubbleObject;

    public GameObject itemBubbleObject;


    public static UIManager Instance
    {
        get { return m_Instance; }
    }

    private void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this;
        }
    }

    private void Start()
    {
        currentLevelText.text =  LevelManager.Instance.GetCurrentLevelWithLevelTextTEMP();
        levelNumberText.text = LevelManager.Instance.GetCurrentLevelWithLevelTextTEMP();// LevelManager.Instance.GetCurrentLevelWithLevelText();
        ButtonInteraction();
    }
    

    private void ButtonInteraction()
    {
       tapButton.onClick.AddListener(delegate
       {
           Gameplay.Instance.StartGame();
       });

        solveButton.onClick.AddListener(delegate
        {
            //This is Temp code.
            //actual is call Gameplay SolveItem function

            Gameplay.Instance.SolveItem();

            //if (currentSelectedID == 0)
            //{
            //    wrongDrag.ReturnItemToInitPos();
            //}
            //else
            //{
            //    dragObject.PlaySuccess();
            //}
	    });
    }
    public IEnumerator TapButtonRoutine()
    {
        yield return new WaitForEndOfFrame();
        startPanelAnimator.SetTrigger(EXIT);
        yield return new WaitForSeconds(0.25f);

        upperPanelAnimator.SetTrigger(ENTRY);
        yield return new WaitForSeconds(0.3f);
        objectiveAnimator.SetTrigger(ENTRY);
    }
    
    public void ShowLevelComplete()
    {
        levComBackImage.enabled = true;
        levelCompleteAnimator.SetTrigger(ENTRY);

        LionKitManager.Instance.LevelCompleted(LevelManager.Instance.GetCurrentLevelTEMP());

        LevelManager.Instance.IncreaseGameLevelTEMP();
        LevelManager.Instance.IncreaseGameLevel();
        //levelCompleteParticle.Play();

        collectButton.onClick.AddListener(delegate
        {
            Gameplay.Instance.GotoNextLevel();
        });
    }

    public void AppearSolveButton()
    {
        solveButtonAnimator.SetTrigger(ENTRY);
    }
    public void DisappearSolveButton()
    {
        solveButtonAnimator.SetTrigger(EXIT);
    }
    public void AppearThanksThought()
    {
        thoughtBubbleObject.SetActive(true);
    }
    public void DisappearThanksThought()
    {
        thoughtBubbleObject.GetComponent<Animator>().SetTrigger(EXIT);
    }

    public void AppearItemThought()
    {
        itemBubbleObject.SetActive(true);
        itemBubbleObject.GetComponent<Animator>().SetTrigger(ENTRY);
    }
    public void DisappearItemThought()
    {
        if (!itemBubbleObject.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Exit"))
        {
            Debug.Log("Jumping");
            itemBubbleObject.GetComponent<Animator>().SetTrigger(EXIT);
        }
    }
}
