using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimationEvent : MonoBehaviour
{
    public UnityEvent OnAnimatonFinishedEvent;

    public void AnimationFinished()
    {
        OnAnimatonFinishedEvent.Invoke();
    }
}
