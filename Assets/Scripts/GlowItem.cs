using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlowItem : MonoBehaviour
{
    public List<Material> glowMatList;

    public Color _emissionColorValue;
     public float _intensity;

    
    void Update()
    {

        _intensity = Mathf.PingPong(Time.time, 3.0f);
        Debug.Log(_intensity);
        
        glowMatList[0].SetVector("_EmissionColor", _emissionColorValue * _intensity);
    }
}
