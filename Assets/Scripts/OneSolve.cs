using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class OneSolve : MonoBehaviour,ISolve
{
    public ParticleSystem trayParticle;
    public ParticleSystem cageParticle;
    public GameObject solveKey;

    public Animator cageGateAnimator;

    public GameObject handObject;
    [Range(0f, 5f)] public float glowIntensity;
    public float floor = 0.3f;
    public float ceiling = 1.0f;
    public Animator handInfo;

    public UnityEvent OnMiddleEvents;
    public UnityEvent OnSolveFinish;

    public void Initialize()
    {
        AgentJerryController.Instance.PlaySeekingHelp();
        InvokeRepeating("TutInvoke",1,3);
    }

    public void SolveLevel()
    {
        StartCoroutine(SolveRoutine());
    }

    public void WrongMessage()
    {
        AgentJerryController.Instance.PlayIncorrect();
    }

    private IEnumerator SolveRoutine()
    {
        DeleteInvoke();

        trayParticle.Play();
        yield return new WaitForSeconds(0.1f);
        OnMiddleEvents.Invoke();
        yield return new WaitForSeconds(0.5f);
        solveKey.SetActive(true);
        yield return new WaitForSeconds(1f);
        cageParticle.Play();
        solveKey.SetActive(false);
        yield return new WaitForSeconds(0.25f);
        cageGateAnimator.SetTrigger("entry");
        yield return new WaitForSeconds(0.7f);

        OnSolveFinish.Invoke();
    }

    //private void Update()
    //{
    //    Image renderer = handObject.GetComponent<Image>();
    //    Material mat = renderer.material;

    //    glowIntensity = floor + Mathf.PingPong(Time.time, ceiling - floor);
    //    Color baseColor = Color.grey; //Replace this with whatever you want for your base color at emission level '1'

    //    Color finalColor = baseColor * Mathf.LinearToGammaSpace(glowIntensity);

    //    mat.SetColor("_EmissionColor", finalColor);

    //}

    public void DeleteInvoke()
    {
        handObject.SetActive(false);

        if (!handInfo.GetCurrentAnimatorStateInfo(0).IsName("Exit"))
        {
            handInfo.SetTrigger("exit");
        }
        
        CancelInvoke();
    }

    public void TutInvoke()
    {
        StartCoroutine(HandTutorialRoutine());
    }
    public IEnumerator HandTutorialRoutine()
    {
        handObject.SetActive(true);
        handInfo.SetTrigger("entry");
        yield return new WaitForSeconds(1f);
        handObject.SetActive(false);
        handInfo.SetTrigger("exit");
    }
}
