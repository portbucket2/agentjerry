using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ItemSlot : MonoBehaviour
{
    public bool isOccupied;
    public Drag3D drag3D;

    public string currentSelected;

    private int ENTRY = Animator.StringToHash("entry");
    private int EXIT = Animator.StringToHash("exit");

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("DragItem"))
        {
            drag3D = other.GetComponent<Drag3D>();
            if(drag3D != null && !isOccupied)
            {
                drag3D.SetOnTriggerPos(transform.position);
                isOccupied = true;
                currentSelected = other.name;

                Gameplay.Instance.AddToList(drag3D);
                //OnPlaceOnSlot.Invoke(drag3D);

                Debug.Log("ETNRY");
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (currentSelected == other.name)
        {
            isOccupied = false;
            Debug.Log("EXIT");

            drag3D.isSetOnTray = false;
            drag3D.EnableAnimation();

            drag3D = null;
        }
    }


    public void AppearTray()
    {
        gameObject.SetActive(true);
        GetComponent<Animator>().SetTrigger(ENTRY);
    }
    public void DisappearTray()
    {
        GetComponent<Animator>().SetTrigger(EXIT);
    }

}
