using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TwoSolve : MonoBehaviour, ISolve
{
    public ParticleSystem trayParticle;
    public ParticleSystem itemParticle;
    public GameObject solveItem;
    public GameObject psudoItem;


    [Space]
    [Header("Agent con")]
    private int THINKING = Animator.StringToHash("thinking");
    private int WRONG = Animator.StringToHash("wrong");
    private int SEEKING_HELP = Animator.StringToHash("seekinghelp");
    private int SUCCESS = Animator.StringToHash("success");

    public Animator agentAnimator;


    public UnityEvent OnMiddleEvents;
    public UnityEvent OnSolveFinish;

    
    public void StartAgentBehaviour()
    {
        InvokeRepeating("AgentBehaviour", 2.0f, 4f);
    }

    private void AgentBehaviour()
    {
        int t_RandValue = Random.Range(0, 2);

        Debug.Log(t_RandValue);
        UIManager.Instance.DisappearItemThought();

        switch (t_RandValue)
        {
            case 0:
                agentAnimator.SetTrigger(THINKING);
                UIManager.Instance.AppearItemThought();
                break;
            case 1:
                agentAnimator.SetTrigger(SEEKING_HELP);
                break;
            

            default:
                break;
        }
    }

    public void SolveLevel()
    {
        StartCoroutine(SolveRoutine());
    }
    private IEnumerator SolveRoutine()
    {
        trayParticle.Play();
        yield return new WaitForSeconds(0.1f);

        OnMiddleEvents.Invoke();
        yield return new WaitForSeconds(0.5f);

        UIManager.Instance.DisappearItemThought();
        CancelInvoke();

        solveItem.SetActive(true);
        yield return new WaitForSeconds(1f);
        itemParticle.Play();
        solveItem.SetActive(false);
        psudoItem.SetActive(true);


        yield return new WaitForSeconds(0.5f);
        OnSolveFinish.Invoke();

        agentAnimator.SetTrigger(SUCCESS);
        yield return new WaitForSeconds(1f);

        UIManager.Instance.AppearThanksThought();
        yield return new WaitForSeconds(2f);
        UIManager.Instance.ShowLevelComplete();

    }
    public void ActivateSolveItem()
    {
        solveItem.SetActive(true);
    }
    public void DeactivateSolveItem()
    {
        solveItem.SetActive(false);
    }

    public void Initialize()
    {
        StartAgentBehaviour();
    }

    public void WrongMessage()
    {
        agentAnimator.SetTrigger(WRONG);
    }

}
