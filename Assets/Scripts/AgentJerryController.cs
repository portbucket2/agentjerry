using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AgentJerryController : MonoBehaviour
{
    public static AgentJerryController Instance;

    private int SEEKING_HELP = Animator.StringToHash("seekinghelp");
    private int CORRECT = Animator.StringToHash("1_correct");
    private int INCORRECT = Animator.StringToHash("1_incorrect");
    private int WALKING = Animator.StringToHash("walking");


    [Space]
    public Animator agentAnimator;

    public List<GameObject> posList;


    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    public void PlaySeekingHelp()
    {
        agentAnimator.SetTrigger(SEEKING_HELP);
    }
    public void PlayCorrect()
    {
        agentAnimator.SetTrigger(CORRECT);
    }
    public void PlayIncorrect()
    {
        agentAnimator.SetTrigger(INCORRECT);
    }
    public void PlayWalking()
    {
        agentAnimator.SetTrigger(WALKING);
    }

    public void RescueFromCage()
    {
        PlayWalking();
        StartCoroutine(XAxisMoveRoutine(posList[0].transform.position, posList[1],
            delegate
            {
                PlayCorrect();
                Gameplay.Instance.LevelComplete();
    	    }
            ));
    }


    public IEnumerator XAxisMoveRoutine(Vector3 t_Dest, GameObject t_TargetTrans, UnityAction t_Action = null)
    {
        float t_Progression = 0f;
        float t_Duration = 1.5f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = transform.position;
        Vector3 t_DestValue = t_TargetTrans.transform.position;// new Vector3(2.16f,1.589f,-.238f);


        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);

            transform.position = new Vector3(
                Mathf.Lerp(t_CurrentFillValue.x, t_DestValue.x, t_Progression),
                Mathf.Lerp(t_CurrentFillValue.y, t_DestValue.y, t_Progression),
                Mathf.Lerp(t_CurrentFillValue.z, t_DestValue.z, t_Progression));

            if (t_Progression >= 1f)
            {
                if (t_Action != null)
                {
                    t_Action.Invoke();
                }

                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(XAxisMoveRoutine(Vector3.zero, null, null));
    }

}
